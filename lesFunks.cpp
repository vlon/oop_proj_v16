#include "lesFunks.h"
#include <iostream>
#include <cstring>
#include "timer4.h"
#include <cctype>
#include "globalVar.h"
#include "const.h"

using namespace std;

// -----------------------------LES-FUNKSJONER--------------------------------- 
// Fjerner mellomrom i en tekst:
void removeSpaces(char s[]) {
	// Lager en temp t-string på samme størrelse som s-string.
	char* t = new char[strlen(s)+1];
	// Skriv over eventuell søppel som måtte befinne seg i t.
	strcpy(t,s);
	int j = 0;
	// Går igjennom s-stringen:
	for (int i = 0; i < strlen(s); i++) {
		// Hvis s-string sin char er ulik mellomrom.
		if (s[i] != ' ') {
			// Legg s-stringen sin char til i t-string.
			t[j++] = s[i];
		}												
	}
	// Kopier stringen t over til stringen s.
	strcpy(s,t);
	// Frigjør plassen til t.
	delete[] t;
}			

// Returnerer samlet verdi av de to første char:
int lesKommando() {
	// Skriver ut ledetekst til bruker.
	cout << "\nKOMMANDO: ";
	// Char variable for kommando.
	char kom[STRLEN];
	// Leser inn en string fra bruker.
	cin.getline(kom,STRLEN);
	// Hvis strlen er bare 1.
	if (strlen(kom) == 1) return toupper(kom[0]);
	// Fjerner mellomrom.
	removeSpaces(kom);
	// Eller så returnerer den begge char-ene.	
 	return toupper(kom[0])+toupper(kom[1]);
}

// Leser tekst ulik 0:
void les(const char* t, char s[]) {
  do  {
	// Skriver ledetekst.
    cout << "\n\t" << t << ": ";
	// Leser inn tekst.
    cin.getline(s, STRLEN);
	// Sjekker at tekstlengden er ulik 0.
  } while (strlen(s) == 0);
}

// Leser inn en enkel char:
void les(const char* t, char &ch) {
	// Skriver ledetekst.
    cout << "\t" << t << ": ";
	// Leser inn char.
    cin >> ch; cin.ignore();
    ch = toupper(ch);
}

long long int lesTidspunkt(const char t[]) {

	// Hent nåværende tid..
	int dag,mnd,aar,tim,min;

	timer_ptr = new Timer();
	timer_ptr->hent(dag,mnd,aar,tim,min);

	int minTid = 0;
	int maxTid = 2359;

	int minDato = 0;
	int maxDato = 161231;

	// Les inn tidspunkt.
	cout << "\n\t" << t;
	int tid = les("Tid", minTid, maxTid);
	int dato = les("Dato", minDato, maxDato);

	return dato*10000+tid;
}

// Leser inn et tall mellom MIN og MAX:
int les(const char t[], int min, int max)  {
  int tall;
  // Fortsett å spør så lenge tall valgt er utenfor MIN/MAX.
  do  {

	// Skriver ut medsendt tekst og intervall:
    cout << "\n\t" << t << " (" << min 							
    	 << '-' << max <<  "):  ";
	// Leser inn tall sålenge innleste tall ikke er tall.
    while(!(cin >> tall)){
        cin.clear();
        cin.ignore(256, '\n');
        cout << "\tMå være tall. Prøv på nytt: ";
    }
    cin.ignore();
  } while ((tall < min  ||  tall > max));

  // Returnerer tallet.
  return tall;
}
