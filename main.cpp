#include <iostream>
#include "Kunder.h"
#include "Kunde.h"
#include "const.h"
#include <cstring>
#include "globalVar.h"
#include "lesFunks.h"
#include "Kategorier.h"
#include "Gjenstander.h"
#include "timer4.h"

using namespace std;

void skrivMeny();
void AS_modus();

int main() {
	// Vis brukermeny:
	skrivMeny();

	// Spør bruker om kommando.
	int kommando = lesKommando();

	// Loop sålenge 'Q' ikke er valgt.
	while(kommando != 'Q') {

		// Lager nye objekter:
		kunder_ptr 		= new Kunder();
		kategorier_ptr 	= new Kategorier();
		gjenstander_ptr = new Gjenstander();

		switch(kommando) {
			// Registrer ny kunde.
			case 'K'+'R' : kunder_ptr->nyKunde(); 				break;
			// Log inn.
			case 'K'+'L' : kunder_ptr->logInn();				break;
			// Log ut.
			case 'K'+'U' : kunder_ptr->logUt();					break;
			// Se egne kjøp og salg.
			case 'K'+'S' : kunder_ptr->seKjoptSolgt(); 			break;
			// Betal vunnet gjenstand/auksjon.
			case 'K'+'B' : kunder_ptr->betalGjenstand();		break;
			// Gi annen kunde tilbakemelding.
			case 'K'+'F' : kunder_ptr->giTilbakemelding();		break;
			// Endre rettigheter til kunde.
			case 'K'+'E' : kunder_ptr->endreRettigheter();		break;
			// Legger til ny hovedkategori.
			case 'A'+'H' : kategorier_ptr->nyKategori();		break;
			// Legger til ny underkategori.
			case 'A'+'U' : kategorier_ptr->nyUnderkategori();	break;
			// AS-modus.
			case 'A'+'S' : AS_modus();                          break;

			// Sletter objekter:
			delete kunder_ptr;	
			delete kategorier_ptr;
			delete gjenstander_ptr;
			
		}			
		// Vis brukermeny:
		skrivMeny();
		// Spør om ny kommando:
		kommando = lesKommando();

	}			
	// Avslutter programmet.
	return 0;
}

// -------------------------------------------------- FUNKSJONER --------------
// Skriver ut brukermenyen:
void skrivMeny() {

	cout << "\n\n...........MENY...........\n\n";

	if (!innlogget) {

		cout << "K R: Registrer\n"
			 << "K L: Logg inn\n"
			 << "A S: Se auksjoner\n";

	} else {

		cout << "K U: Logg ut kunde\n"
			 << "K S: Se egne kjoep og salg\n"
			 << "K B: Betale vunnet auksjon\n"
			 << "K F: Gi en annen kunde feedback/tilbakemelding\n"
			 << "A S: Se auksjoner\n";

		if (admin) {

			cout << "K E: Endre annen kundes rettigheter\n"
				 << "A H: Legge inn ny hovedkategori\n"
				 << "A U: Legge inn ny underkategori\n";
		}
	}

	cout << "Q  : AVSLUTT\n";
}

// AS-Modus:
void AS_modus() {

	    char kat[STRLEN] = "";
		char ukat[STRLEN] = "";
		kategorier_ptr->velgKategori(kat);
		if (strlen(kat) != 0) {
			kategorier_ptr->velgUnderkategori(kat, ukat);
		}
		if (strlen(ukat) != 0) {
			cout << "\n\n......MENY(AS-modus)......\n\n";
			cout << "\nAS-modus i underkategori: " << ukat << "\n\n";
			cout << "A  : Se alle aktive auksjonsgjenstander\n"
				 << "E  : Se alle detaljer om en gjenstand\n";

			if (innlogget) {
				cout << "L  : Legg inn ny gjenstand\n"
					<< "B  : Legg inn bud paa gjenstand\n";
			}
			cout << "Q  : AVSLUTT\n";
			// Hent ut hovedkategori.
			kat_ptr = (Kategori*)kategorier_ptr->hentKatListe()->remove(kat);
			// Hent underkategori.
			ukat_ptr = (Underkategori*)kat_ptr->hentUkatListe()->remove(ukat);
			// Les inn gjenstander fra fil.
			ukat_ptr->lesGjenstanderFraFil();
			// Legg den kategori tilbake i listen.
			kategorier_ptr->hentKatListe()->add(kat_ptr);
			// Legg underkategorien tilbake i listen.
			kat_ptr->hentUkatListe()->add(ukat_ptr);

			// Spør bruker om kommando.
			int kommando = lesKommando();
			// Loop sålenge 'Q' ikke er valgt.
			while (kommando != 'Q') {
				switch (kommando) {
					// Se alle gjenstander i underkategori.
					case  	 'A': kategorier_ptr->visGjenstander(kat, ukat);  break;
					// Se alle detaljer om gjenstand.
					case  	 'E': kategorier_ptr->visGjenstand(kat, ukat);	  break;
					// Hvis innlogget:
					if (innlogget) {
					// Legg inn ny gjenstand i underkategori.
					case  	 'L': gjenstander_ptr->nyGjenstand(kat, ukat);	  break;
					// Legg inn bud på gjenstand.
					case  	 'B': gjenstander_ptr->nyttBud(kat, ukat);		  break;
						}
				}
				cout << "\n\n......MENY(AS-modus)......\n\n";
				cout << "\nAS-modus i underkategori: " << ukat << "\n\n";
				cout << "A  : Se alle aktive auksjonsgjenstander\n"
					 << "E  : Se alle detaljer om en gjenstand\n";

				// Hvis innlogget:
				if (innlogget) {
					cout << "L  : Legg inn ny gjenstand\n"
						 << "B  : Legg inn bud paa gjenstand\n";
				}
				cout << "Q  : AVSLUTT\n";
				// Spør bruker om kommando.
				kommando = lesKommando();
			}
		}
		ukat_ptr->skrivGjenstanderTilFil();
}
