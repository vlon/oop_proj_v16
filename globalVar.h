#ifndef __GLOBALVAR_H
#define __GLOBALVAR_H

#include "Gjenstand.h"
#include "Gjenstander.h"
#include "Kategori.h"
#include "Kategorier.h"
#include "Underkategori.h"
#include "Kunde.h"
#include "Kunder.h"
#include "Bud.h"

extern bool innlogget;
extern bool admin;

extern char ch;

extern class Gjenstand* gjenstand_ptr;
extern class Kategori* kat_ptr;
extern class Underkategori* ukat_ptr;
extern class Kunde* kunde_ptr;

extern class Kunder* kunder_ptr;
extern class Kategorier* kategorier_ptr;
extern class Gjenstander* gjenstander_ptr;
extern class Bud* bud_ptr;
extern class Timer* timer_ptr;
#endif