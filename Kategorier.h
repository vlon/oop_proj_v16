#ifndef __KATEGORIER_H
#define __KATEGORIER_H

#include "ListTool2B.h"
#include "const.h"

// Klasse for hovedkategorier.
class Kategorier {

private:

	static int kategoriNr, gjenstandNr;     // INT-er for siste i bruk.
	List* kategoriListe;				    // Liste for hovedkategorier.

public:

	// Constructor.
	Kategorier();
	// Destructor.
	~Kategorier();
	// Henter kategoriliste.
	List* hentKatListe();
	// Ny kategori.
	void nyKategori();
	// Ny underkategori.
	void nyUnderkategori();
	// Skriver alle kategorier.
	void display();
	// Leser kategori/underkategori fra fil.
	void lesKategorierFraFil();
	// Skriv kategori/underkategori til fil.
	void skrivKategorierTilFil();
	// Velge en bestemt kategori.
	void velgKategori(char* katValg);
	// Velge en bestemt underkategori.
	void velgUnderkategori(char* katValg, char* ukatValg);
	// Viser alle gjenstander i en underkategori.
	void visGjenstander(char* kat, char* ukat);
	// Viser en valgt gjenstand i en underkategori.
	void visGjenstand(char* kat, char* ukat);
};
#endif