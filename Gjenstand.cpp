#include <iostream>
#include <fstream>
#include "lesFunks.h"
#include <cmath>
#include "const.h"
#include "Gjenstand.h"
#include "timer4.h"
#include "globalVar.h"

using namespace std;

// Gjenstand:
Gjenstand::Gjenstand(int gjNr) : NumElement(gjNr) {
	budListe = new List(Sorted);
	les("Gjenstandstittel", gjTittel);
	les("Beskrivelse", beskrivelse);
	strcpy(selgerNavn, kunder_ptr->hentInnloggetBruker());
	startTidspkt = lesTidspunkt("Startstidspunkt");
	sluttTidspkt = lesTidspunkt("Slutttidspunkt");
	startPris 		= les("StartPris", 0, MAXPRIS);
	porto 			= les("Porto", 0, MAXPORTO);
	budOkn 			= les("Budøkning", 0, MAXOKNING);
	antBud = 0;
	gjeldendeBud = 0;
	// Informasjonsmelding.
	cout << "\n\tLagt til ny gjenstand med gjenstandsnummer " << gjNr << ".\n\n";
}

// Skriver ut 'delvis' informasjon om en gjenstand:
void Gjenstand::display() {
	if (aktiv) {					
		cout << "\n\t" << "(" << number << ") \t" << gjTittel
			<< "\n\tSelger: " << selgerNavn
			<< "\n\tPris: \t" << hentgjeldendeBud() << ",-\n";
	}
}

// Skriver ut all informasjon om en gjenstand:
void Gjenstand::displayAllInfo() {
	cout << "\n\tSelgers navn:\t"
		<< selgerNavn << "\n\tBeskrivelse:\t"
		<< beskrivelse << "\n\tTittel:\t\t"
		<< gjTittel << "\n\tStart:\t";
	timer_ptr->displayTidspunkt(startTidspkt);
	cout << "\tSlutt:\t";
	timer_ptr->displayTidspunkt(sluttTidspkt);
	cout << "\tStartpris:\t"
		<< abs(startPris) << ",-\n\tPorto:\t\t"
		<< porto << ",-\n\tBudokning:\t"
		<< budOkn << ",-\n";
}

void Gjenstand::visBud() {
											
	// Går igjennom alle bud til gjenstanden
	if (budListe->noOfElements() > 0) {
		for (int i = 1; i <= budListe->noOfElements(); i++) {
			// Henter bud (fjerner fra listen).
			bud_ptr = (Bud*)budListe->removeNo(i);
			// Display underkategori.
			bud_ptr->display();
			// Legger underkategorien tilbake i listen.
			budListe->add(bud_ptr);
		}
	}
	else {
		cout << "\t(Ingen bud)" << endl;
	}
}

void Gjenstand::sjekkAktiv() {
	int dag, mnd, aar, tim, min;

	//Henter nåværende tidspunkt.
	timer_ptr = new Timer();
	timer_ptr->hent(dag, mnd, aar, tim, min);

	int tidnaa = timer_ptr->konverterFraTm(dag, mnd, aar, tim, min);

	if ((tidnaa >= startTidspkt) && (tidnaa < sluttTidspkt)) {
		aktiv = true;
	}
	else { 
		aktiv = false;
	}
}

// Skriver gjenstand til fil:
void Gjenstand::skrivTilFil(ofstream& ut) {
	ut  << number << '\n'
		<< gjTittel << '\n'
		<< beskrivelse << '\n'
		<< selgerNavn << '\n'
		<< startTidspkt << '\n'		
		<< sluttTidspkt << '\n'		
		<< (startPris * -1) << '\n'
		<< porto << '\n'
		<< budOkn << '\n'
		<< antBud << '\n';

	for (int i = 1; i <= budListe->noOfElements(); i++) {

		// Henter bud fra listen(fjernes fra listen)
		bud_ptr = (Bud*)budListe->removeNo(i);

		// Budet skriver seg selv til fil
		bud_ptr->skrivTilFil(ut);

		// Legg bud tilbake i listen
		budListe->add(bud_ptr);
	}

}

// Leser gjenstand fra fil:
Gjenstand::Gjenstand(ifstream& inn, int gjNr) : NumElement(gjNr) {
	// Ny liste for bud.
	budListe = new List(Sorted);
	// Leser inn all data:
	inn.getline(gjTittel, STRLEN);
	inn.getline(beskrivelse, STRLEN);
	inn.getline(selgerNavn, STRLEN);
	inn >> startTidspkt >> sluttTidspkt >> startPris >> porto >> budOkn;
	inn.ignore();
	inn >> antBud; inn.ignore();
	gjeldendeBud = 0;

	int bud;
	for (int i = 0; i < antBud; i++) {
		// Les inn bud
		inn >> bud; inn.ignore();
		bud_ptr = new Bud(inn, bud);
		budListe->add(bud_ptr);
		if (abs(bud) > gjeldendeBud) {
			gjeldendeBud = abs(bud);
		}
	}

	sjekkAktiv();
}

void Gjenstand::auksjonFerdig() {
	int dag, mnd, aar, tim, min;
	timer_ptr = new Timer();
	timer_ptr->hent(dag, mnd, aar, tim, min);
	int tidnaa = timer_ptr->konverterFraTm(dag, mnd, aar, tim, min);
	if (tidnaa > sluttTidspkt) {
		char brukerKjop[STRLEN];
		// Henter kjøper fra største bud
		strcpy(brukerKjop, hentKjoper(number));
		//Legger til et salg hos selger
		kunder_ptr->addAntKjop(brukerKjop);
		//Legger til et salg hos selger
		kunder_ptr->addAntSalg(selgerNavn);
		//Skriver endringer i kunder-strukturen.
		kunder_ptr->skrivKjoptSolgtTilFil(selgerNavn, brukerKjop, gjTittel,  sluttTidspkt, gjeldendeBud, porto, number);
	}
}

const char* Gjenstand::hentKjoper(int gjNr) {
	// Hent ut den første i listen, returner navnet til det brukeren

	char temp[STRLEN];

	// Hent ut den første i budlisten
	bud_ptr = (Bud*)budListe->removeNo(1);
					
	strcpy(temp, bud_ptr->hentBruker());

	// Legg bud tilbake i listen.
	budListe->add(bud_ptr);

	// returner temp
	return temp;
}

List* Gjenstand::hentBudListe() {
	return budListe;
}
bool Gjenstand::hentAktiv() {
	return aktiv;
}

int Gjenstand::hentStartPris() {
	return startPris;
}

int  Gjenstand::hentbudOkn() {
	return budOkn;
}

int  Gjenstand::hentgjeldendeBud() {
	if (antBud == 0) {
		gjeldendeBud = abs(startPris);
    	return gjeldendeBud;
	} else {
		return gjeldendeBud;
	}
}

const char* Gjenstand::hentNvn() {	
	return selgerNavn;
}

void  Gjenstand::settGjeldendeBud(int bud) {
	gjeldendeBud = bud;
}

int Gjenstand::hentAntBud() {
	return antBud;
}

void  Gjenstand::oekAntBud() {
	antBud++;
}