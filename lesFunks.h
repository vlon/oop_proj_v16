#ifndef __LESFUNKS_H
#define __LESFUNKS_H

void removeSpaces(char s[]);
int lesKommando();	
void les(const char t[], char s[]);
long long int lesTidspunkt(const char t[]);
void les(const char* t, char &s);
int les(const char t[], int min, int max); 							// Leser inn et tall mellom min og max.

#endif