#include <iostream>
#include <cstring>
#include <string>
#include <cmath>
#include <fstream>
#include "Bud.h"
#include "lesFunks.h"
#include "timer4.h"
#include "Kunder.h"
#include "lesFunks.h"
#include "Gjenstand.h"
#include "Underkategori.h"
#include "globalVar.h"
using namespace std;
Bud::Bud() {}

Bud::Bud(int pris) : NumElement(pris) {
	// Henter innlogget bruker
	strcpy(brukernavn,kunder_ptr->hentInnloggetBruker()) ;
	// Henter nĺvćrende tidspunkt
	timer_ptr = new Timer();
	timer_ptr->hent(dag, mnd, aar, tim, min);
	// Konverter til dato med aar fřrst
	tidspunkt = timer_ptr->konverterFraTm(dag, mnd, aar, tim, min);
	display();
}

void Bud::display() {
	cout << "\n\t\tBud:       \t" << abs(number) << ",-\n"
		 << "\t\tBrukernavn:\t" << brukernavn << '\n'
		 << "\t\tTidspunkt: "; timer_ptr->displayTidspunkt(tidspunkt);
}

void Bud::skrivTilFil(ofstream& ut) {
	if (number < 0) {
		ut << number << '\n';
	}
	else {
		ut << (number * -1) << '\n';
	}
	ut  << brukernavn << '\n'
		<< tidspunkt << '\n';
}

Bud::Bud(ifstream &inn, int pris) : NumElement(pris) {
	inn.getline(brukernavn, STRLEN);
	inn >> tidspunkt; inn.ignore();
}

char* Bud::hentBruker() {
	return brukernavn;
}