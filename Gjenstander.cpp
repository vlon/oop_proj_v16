// GJENSTANDER.CPP
#include <iostream>
#include <fstream>
#include <string>
#include "Gjenstander.h"
#include "Gjenstand.h"
#include "const.h"
#include "globalVar.h"
#include "Underkategori.h"
#include "Kategori.h"
#include "Kategorier.h"
#include "lesFunks.h"

using namespace std;

int Gjenstander::gjNr = 1;

Gjenstander::Gjenstander() {}

void Gjenstander::nyGjenstand(char* kat, char* ukat) {

	// Hvis brukeren er innlogget:
	if (innlogget) {

		// Henter ut hovedkategori(fjernes fra listen)
		kat_ptr = (Kategori*)kategorier_ptr->hentKatListe()->remove(kat);
		// Henter underkategori(fjernes fra listen)
		ukat_ptr = (Underkategori*)kat_ptr->hentUkatListe()->remove(ukat);

		// Lag en ny gjenstand
		gjenstand_ptr = new Gjenstand(gjNr++);

		// Legg gjenstanden til i listen
		ukat_ptr->hentGjenstandsListe()->add(gjenstand_ptr);

		// Legg kategori tilbake i listen
		kategorier_ptr->hentKatListe()->add(kat_ptr);
		// Legg underkategorien tilbake i listen
		kat_ptr->hentUkatListe()->add(ukat_ptr);

		// Skriver gjenstander til fil
		ukat_ptr->skrivGjenstanderTilFil();
	}
}

// Setter gjenstandsnr. lik medsent INT:
void Gjenstander::settGjNr(int nr) { gjNr = nr; }

void Gjenstander::nyttBud(char* kat, char* ukat) {

	// Henter ut hovedkategori(fjernes fra listen)
	kat_ptr = (Kategori*)kategorier_ptr->hentKatListe()->remove(kat);
	// Henter underkategori(fjernes fra listen)
	ukat_ptr = (Underkategori*)kat_ptr->hentUkatListe()->remove(ukat);

	// Viser alle tilgjengelige gjenstander
	ukat_ptr->displayGjenstander();

	// Les inn gjenstandsnummer
	int nr;
	do {
		cout << "\n\tGjenstandsnr: "; cin >> nr; cin.ignore();
	} while (!ukat_ptr->hentGjenstandsListe()->inList(nr));

	// Hent gjenstanden(fjernes fra listen)
	gjenstand_ptr = (Gjenstand*)ukat_ptr->hentGjenstandsListe()->removeNo(nr);

	// Hvis innlogget bruker og selgers navn er ulike:
	if (strcmp(kunder_ptr->hentInnloggetBruker(), gjenstand_ptr->hentNvn()) != 0) {
		// Hvis auksjonen/gjenstanden er aktiv:
		if (gjenstand_ptr->hentAktiv()) {

			// Henter informasjon om:
			int gjeldendeBud = gjenstand_ptr->hentgjeldendeBud();
			int budOkn = gjenstand_ptr->hentbudOkn();
			int antBud = gjenstand_ptr->hentAntBud();
			int startPris = gjenstand_ptr->hentStartPris();
		
			// Sett minstebud
			int minsteBud = 0;
			(antBud == 0) ? minsteBud = startPris : minsteBud = gjeldendeBud+budOkn;
		
			// Les inn bud
			int nyttBud = les("Ditt bud: ", abs(minsteBud), MAXPRIS);
			
			// Øker antall bud med 1.
			gjenstand_ptr->oekAntBud();
			// Setter bud som nåværende gjeldende bud
			gjenstand_ptr->settGjeldendeBud(nyttBud);
		
			// Opprett nytt bud
			bud_ptr = new Bud(nyttBud * -1);
		
			// Legg bud i listen
			gjenstand_ptr->hentBudListe()->add(bud_ptr);
		}
	} else {
			cout << "\n\tIKKE tillatt aa by paa egne gjenstander!" 
			<<" (trykk 'Enter for aa fortsette)."; cin.get();
	}
	// Legg gjenstand tilbake i listen
	ukat_ptr->hentGjenstandsListe()->add(gjenstand_ptr);
	// Skriv endringer til fil
	ukat_ptr->skrivGjenstanderTilFil();
	// Legg underkategorien tilbake i listen
	kat_ptr->hentUkatListe()->add(ukat_ptr);
	// Legg kategori tilbake i listen
	kategorier_ptr->hentKatListe()->add(kat_ptr);
}
	