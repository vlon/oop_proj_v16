#include <ctime>
#include "timer4.h"      //  Timer-klassen
#include <iostream>
#include <iomanip>
using namespace std;

Timer::Timer(){}

void Timer::hent(int& dag, int& mnd, int& aar, int& tim, int& min) {



#ifdef OS_WINDOWS
	__time64_t  tid;
	_time64(&tid);
	_localtime64_s(tidspunkt, &tid);
#else 
	// MAC
	time_t tid;
	time(&tid);
	tidspunkt = localtime(&tid);
#endif

	dag = tidspunkt->tm_mday;
	mnd = tidspunkt->tm_mon;
	aar = tidspunkt->tm_year;
	tim = tidspunkt->tm_hour;
	min = tidspunkt->tm_min;

}

void Timer::konverterFraDagFrst(long long int& dato) {
	int dag, mnd, aar, tim, min;
	long long int t = dato;
	dag = t / 100000000;
	mnd = (t - (dag * 100000000)) / 1000000;
	aar = (t - ((dag * 100000000) + mnd * 1000000)) / 10000;
	tim = (t - (((dag * 100000000) + mnd * 1000000) + aar * 10000)) / 100;
	min = t % 100;
	konverterFraTm(dag,mnd, aar, tim, min);
}
int Timer::konverterFraTm(int& dag, int& mnd, int& aar, int& tim, int& min) {
	int dato = ((((((aar-100) * 100000000) + (mnd+1) * 1000000) + dag * 10000) + tim * 100) + min);
	return  dato;
}

void Timer::displayTidspunkt(long long int dato) {
	int dag, mnd, aar, tim, min;
	long long int t = dato;
	aar = t / 100000000;
	mnd = (t - (aar * 100000000)) / 1000000;
	dag = (t - ((aar * 100000000) + mnd * 1000000)) / 10000;
	tim = (t - (((aar * 100000000) + mnd * 1000000) + dag * 10000)) / 100;
	min = t % 100;

	cout << "\t" << setfill('0') << setw(2) << dag << "." << setfill('0')
		<< setw(2) << mnd << "." << aar << " " << setfill('0') << setw(2)
		<< tim << ":" << setfill('0') << setw(2) << min << endl;
};