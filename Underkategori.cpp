#include "Kategori.h"
#include "Kategorier.h"
#include "Underkategori.h"
#include "lesFunks.h"
#include "const.h"
#include <iostream>
#include <string>
#include <cstring>


using namespace std;

// Constructor -> char g�r til textelement, int = kategorinr:
Underkategori::Underkategori(const char* txt, int nr): TextElement(txt) {
	ukategoriNr = nr;
	gjenstandsListe = new List(Sorted);
}

// Display underkategori:
void Underkategori::display() {
	// Skriver ut underkategorinr. og navn p� kategori.
	cout << "\t" << text << endl;
}

// Viser all gjenstander i gjenstandslisten:
void Underkategori::displayGjenstander() {

	if (gjenstandsListe->noOfElements() > 0) {

		// G�r igjennom alle gjenstander:
		for (int i = 1; i <= gjenstandsListe->noOfElements(); i++) {

			// Hent ut gjenstand (fjernes fra listen).
			gjenstand_ptr = (Gjenstand*)gjenstandsListe->removeNo(i);

			// Gjenstand skriver seg selv til skjermen.
			gjenstand_ptr->sjekkAktiv();
			if (gjenstand_ptr->hentAktiv()) {
				gjenstand_ptr->display();
			}
			// Legg gjenstand tilbake i listen.
			gjenstandsListe->add(gjenstand_ptr);
		}
	}
	else {
		cout << "\n\t(Ingen aktive auksjoner)" << endl;
	}
}

// Henter underkategorinummer:
int Underkategori::hentKatNr() {
	return ukategoriNr;
}

// Skriv underkategori til fil:
void Underkategori::skrivTilFil(ofstream & ut) {
	// Skriver ut underkategorinr. og navn p� underkategori.
	ut << ukategoriNr << " " << text << '\n';
}

// Henter gjenstandliste:
List* Underkategori::hentGjenstandsListe() {
	return gjenstandsListe;
}

void Underkategori::skrivGjenstanderTilFil() {

	// Vi lager en fil med navnet G<ukat>.DTA
	//string s = ukategoriNr;
	string fil = "G"+to_string(ukategoriNr)+".DTA";
	ofstream ut(fil);

	// Hvis filen kan �pnes:
	if (ut) {
		// Finner og g�r igjennom ant. eksisterende kunder:
		for (int i = 1; i <= gjenstandsListe->noOfElements(); i++) {
			// Hent ut gjenstand(fjernes fra listen).
			gjenstand_ptr = (Gjenstand*)gjenstandsListe->removeNo(i);

			if (!gjenstand_ptr->hentAktiv()) {
				gjenstand_ptr->auksjonFerdig();
			}

			// Ber gjenstand skrive seg selv til fil.
			gjenstand_ptr->skrivTilFil(ut);
			// Legg gjenstand tilbake i listen.
			gjenstandsListe->add(gjenstand_ptr);
		}
	}
}

void Underkategori::lesGjenstanderFraFil() {

	// Vi leser fra en fil med navnet G<ukat>.DTA
	//string s = ukategoriNr;
	string fil = "G"+to_string(ukategoriNr)+".DTA";
	ifstream inn(fil);

	// Hvis filen kan �pnes:
	if (inn) {
		int gjNr = 1;
		// S� lenge neste char i filen ikke er end of file.
		while (inn.peek() != -1) {
			inn >> gjNr; inn.ignore();
			// Lag en ny gjenstand, leser inn egen data fra fil.
			gjenstand_ptr = new Gjenstand(inn,gjNr++);
			// Legger gjenstand til i listen.
			gjenstandsListe->add(gjenstand_ptr);
		}
		// Setter gjenstandsnummer til det siste innleste gjNr.
		gjenstander_ptr->settGjNr(gjNr);
	}
}