#include "Kategorier.h"
#include "Kategori.h"
#include "Underkategori.h"
#include "Gjenstander.h"
#include "lesFunks.h"
#include "const.h"
#include "globalVar.h"
#include <string>
#include <iostream>

using namespace std;

// Setter kategori/gjenstandnr. til å begynne på 10000.
int Kategorier::kategoriNr;
int Kategorier::gjenstandNr;

// Constructor:
Kategorier::Kategorier() {
	// Ny sortert liste for kategorier.
	kategoriListe = new List(Sorted);
}

// Destructor:
Kategorier::~Kategorier() {
	// Fjerner aktuell kategori.
	delete kategoriListe;
}

// Henter kategoriliste:
List* Kategorier::hentKatListe() {
	return kategoriListe;
}

// Ny kategori:
void Kategorier::nyKategori() {

	lesKategorierFraFil();

	// Char for kategorinavn.
	char katNvn[STRLEN];

	// Hvis brukeren er innlogget og ADMIN:
	if (innlogget && admin) {
		// Informasjonsmelding skrives ut.
		cout << "\n\tRegistrering av ny hovedkategori:" 
			 << "\n\n\tEksisterende hovedkategorier:\n" << endl;
		// Skriver ut eksisterende kategorier.
		display();
		// Leser nytt kategorinavn.
		les("Hovedkategorinavn", katNvn);

		// Hvis kategorien finnes:
		if (kategoriListe->inList(katNvn)) {
			// Skriver ut feilmelding.
			cout << "\n\tHovedkategorien '" << katNvn
				<< "' finnes allerede (trykk 'Enter' for aa fortsette).";
			cin.get();
		}
		// Hvis ikke kategorien finnes:
		else {
			// Lager ny kategori, sender navn og øker kategorinr.
			kat_ptr = new Kategori(katNvn, kategoriNr++);
			// Legger kategori i listen.
			kategoriListe->add(kat_ptr);
			// Informasjonsmelding skrives ut.
			cout << "\n\tHovedkategori med navn '" << katNvn
				 << "' er opprettet.\n";
			// Skriver endring til fil.
			skrivKategorierTilFil();
		}
	}
	else {
		// Hvis bruker ikke er innlogget eller ikke er ADMIN.
		cout << "\n\tIngen bruker innlogget/ikke ADMIN rettigheter "
			<< "(trykk 'Enter' for aa fortsette)."; cin.get();
	}
}

// Ny underkategori:
void Kategorier::nyUnderkategori() {

	lesKategorierFraFil();

	// Char for hovedkategori og underkategori.
	char katNvn[STRLEN], ukatNvn[STRLEN];

	if (innlogget && admin) {
		// Informasjonsmelding skrives ut.
		cout << "\n\tRegistrering av ny underkategori:"
			 << "\n\n\tEksisterende hovedkategorier:\n" << endl;
		// Skriver ut eksisterende hovedkategorier.
		display();
		// Leser hovedkategorinavn.
		les("Under hvilken hovedkategori", katNvn);

		// Hvis bruker har valgt gyldig kategori:
		if (kategoriListe->inList(katNvn)) {
			// Hent kategorien (fjernes fra listen).
			kat_ptr = (Kategori*)kategoriListe->remove(katNvn);
			// Informasjonsmelding skrives ut.
			cout << "\n\tEksisterende underkategorier:\n" << endl;
			// SKriver ut eksisterende underkategorier.
			kat_ptr->displayAll();
			// Legg hoverkategorien tilbake i listen.
			kategoriListe->add(kat_ptr);
			// Leser underkategorinavn.
			les("Navn paa underkategori", ukatNvn);
			// Hvis underkategorien ikke finnes:
			if (!kat_ptr->hentUkatListe()->inList(ukatNvn)) {
				// Lager ny underkategori, sender med navn og øker kategorinr.
				ukat_ptr = new Underkategori(ukatNvn, kategoriNr++);
				// Skriver ut melding om at fullført opprettelse.
				cout << "\n\tUnderkategori med navn '" << ukatNvn
					<< "' er opprettet under '" << katNvn << "'.\n" << endl;
				// Legg underkategorien inn i hovedkategorien sin liste.
				kat_ptr->hentUkatListe()->add(ukat_ptr);
				// Skriver endringer til fil.
				skrivKategorierTilFil();
			}
			else {
				// Skriver feilmelding:
				cout << "\n\tUnderkategorien '" << katNvn
					 << "' finnes allerede (trykk 'Enter' for aa fortsette).";
				cin.get();
			}
		}
		else {
			// Skriver feilmelding:
			cout << "\n\tDet eksisterer ingen hovedkategori med navn '"
				 << katNvn << "' (trykk 'Enter' for aa fortsette).";
			cin.get();
		}
	}
	else {
		// Hvis bruker ikke er innlogget eller ikke er ADMIN.
		cout << "\n\tIngen bruker innlogget/ikke ADMIN rettigheter "
			 << "(trykk 'Enter' for aa fortsette)."; cin.get();
	}
}

// Leser kategoriene fra fil:
void Kategorier::lesKategorierFraFil() {

	kategoriNr = 0;

	// Filen som data leses inn fra.
	string fil = "KATEGORIER.DTA";

	ifstream inn(fil);

	// Hvis filen kan åpnes:
	if (inn) {
		// Variable for kategori- og underkategorinavn.
		char katNvn[STRLEN], ukatNvn[STRLEN];
		// Variable for ant. underkategorier, kategori- og underkategorinr.
		int antUKat, katNr, ukatNr;
		// Så lenge det ikke er slutt på filen:
		while (inn.peek() != -1) {
			// Leser inn kategorinr.
			inn >> katNr; inn.ignore();
			// Leser inn kategorinavn.
			inn.getline(katNvn, STRLEN);
			// Oppretter ny kategori, sender med navn og nr.
			kat_ptr = new Kategori(katNvn, katNr);
			// Øker kategorinr.
			kategoriNr++;
			// Legger kategorien inn i listen.
			kategoriListe->add(kat_ptr);
			// Leser ant. underkategorier.
			inn >> antUKat; inn.ignore();
			// Hvis kategorien har noen underkategorier:
			if (antUKat > 0) {
				// Går igjennom alle underkategoriene:
				for (int i = 0; i < antUKat; i++) {
					// Leser inn underkategorinr.
					inn >> ukatNr; inn.ignore();
					// Leser inn underkategorinavn.
					inn.getline(ukatNvn, STRLEN);
					// Oppretter ny underkategori, sender med navn og nr.
					ukat_ptr = new Underkategori(ukatNvn, ukatNr);
					// Øker kategorinr.
					kategoriNr++;
					// Legger underkategorien inn i underkategorilisten.
					kat_ptr->hentUkatListe()->add(ukat_ptr);
				}
			}
		}
	}
}

// Skriver kategoriene til fil:
void Kategorier::skrivKategorierTilFil() {

	// Filen som data skrives til.
	string fil = "KATEGORIER.DTA";

	ofstream ut(fil);

	// Hvis filen kan åpnes:
	if (ut) {
		// Finner og går igjennom ant. eksisterende kategorier:
		for (int i = 1; i <= kategoriListe->noOfElements(); i++) {
			// Hent ut kategori (fjernes fra listen).
			kat_ptr = (Kategori*)kategoriListe->removeNo(i);
			// Kategorien skriver seg selv til fil.
			kat_ptr->skrivTilFil(ut);
			// Legger kategorien tilbake i listen.
			kategoriListe->add(kat_ptr);
		}
	}
}

// Skriver ut info om kategoriene:
void Kategorier::display() {
	if (kategoriListe->noOfElements() > 0) {
		// Finner og går igjennom alle kategoriene:
		for (int i = 1; i <= kategoriListe->noOfElements(); i++) {
			// Hent ut kategori (fjernes fra listen).
			kat_ptr = (Kategori*)kategoriListe->removeNo(i);
			// Kategori skriver seg selv ut.
			kat_ptr->display();
			// Legg kategori tilbake i listen.
			kategoriListe->add(kat_ptr);
		}
	}
	else {
		cout << "\t(Ingen hovedkategorier er opprettet ennaa)" << endl;
	}
}


void Kategorier::velgKategori(char* katValg) {

	// Leser kategorier fra fil.
	lesKategorierFraFil();

	// Hvis det finnes kategorier:
	if (kategoriListe->noOfElements() > 0) {
		cout << "\n\tVelg hovedkategori for AS-modus:\n" << endl;
		// Vis kategorier.
		display();
		// Les inn valg fra bruker.
		do {
			les("Velg kategori", katValg);
		} while (!kategoriListe->inList(katValg) || katValg[0] == 'Q'
			|| katValg[0] == 'q');
	}
	// Hvis det ikke finnes noen kategori:
	else {
		if (innlogget && admin) {
			cout << "\n\t(Ingen hovedkategorier er opprettet ennaa)" << endl;
			les("Vil du opprette ny hovedkategori?(y/n)", ch);
			if (ch == 'Y') {
				// Leser nytt kategorinavn.
				les("Hovedkategorinavn", katValg);
				// Lager ny kategori, sender navn og øker kategorinr.
				kat_ptr = new Kategori(katValg, kategoriNr++);
				// Legger kategori i listen.
				kategoriListe->add(kat_ptr);
				// Informasjonsmelding skrives ut.
				cout << "\n\tHovedkategori med navn '" << katValg
					<< "' er opprettet.\n";
				// Skriver endring til fil.
				skrivKategorierTilFil();
			}
		}
		else {
			cout << "\n\tIngen kategorier er opprettet." << endl;
		}
	}
}


void Kategorier::velgUnderkategori(char* katValg, char* ukatValg) {
	// Hent hovedkategori
	kat_ptr = (Kategori*)kategoriListe->remove(katValg);
	// Hvis det finnes elementer i underkategorilisten:
	if (kat_ptr->hentUkatListe()->noOfElements() > 0) {
		// Informasjonsmelding.
		cout << "\n\tVelg underkategori for AS-modus:\n" << endl;
		// Vis underkategorier
		kat_ptr->displayAll();
		// Legg hovedkategori tilbake i listen.
		kategoriListe->add(kat_ptr);
		// Spør bruker om underkategori.
		do {
			les("Velg underkatergori", ukatValg);
		} while (!kat_ptr->hentUkatListe()->inList(ukatValg)
			|| ukatValg[0] == 'Q' || ukatValg[0] == 'q');
	}
	else { // Hvis det ikke finnes noen underkategori:
		if (innlogget && admin) {
			cout << "\n\t(Ingen underkategorier er opprettet ennaa)" << endl;
			les("Vil du opprette ny underkategori?(y/n)", ch);
			if (ch == 'Y') {
				// Leser inn navn på ny underkategori.
				les("Navn paa underkategori", ukatValg);
				// Lager ny underkategori, sender med navn og øker kategorinr.
				ukat_ptr = new Underkategori(ukatValg, kategoriNr++);
				// Skriver ut melding om at fullført opprettelse.
				cout << "\n\tUnderkategori med navn '" << ukatValg
					<< "' er opprettet under '" << katValg << "'.\n" << endl;
				// Legg underkategorien inn i hovedkategorien sin liste.
				kat_ptr->hentUkatListe()->add(ukat_ptr);
				// Legg hovedkategori tilbake i listen.
				kategoriListe->add(kat_ptr);
				// Skriver endringer til fil.
				skrivKategorierTilFil();
			}
		}
		else {
			cout << "\n\tIngen underkategorier er opprettet." << endl;
			// Legg hovedkategori tilbake i listen.
			kategoriListe->add(kat_ptr);
		}
	}
}

// Viser 'delvis' informasjon om alle gjenstander i en underkategori:
void Kategorier::visGjenstander(char* kat, char* ukat) {
	// Hent ut hovedkategori.
	kat_ptr = (Kategori*)kategorier_ptr->hentKatListe()->remove(kat);
	// Hent underkategori.
	ukat_ptr = (Underkategori*)kat_ptr->hentUkatListe()->remove(ukat);
	// Viser alle gjenstander til bruker
	ukat_ptr->displayGjenstander();
	// Legg den kategori tilbake i listen.
	kategorier_ptr->hentKatListe()->add(kat_ptr);
	// Legg underkategorien tilbake i listen.
	kat_ptr->hentUkatListe()->add(ukat_ptr);

}

// Viser all informasjon om en valgt gjenstand:
void Kategorier::visGjenstand(char* kat, char* ukat) {
	// INT for valg av gjenstand.
	int valg;

	// Fjerner kategorien fra listen.
	kat_ptr = (Kategori*)kategorier_ptr->hentKatListe()->remove(kat);
	// Fjerner underkategori fra listen.
	ukat_ptr = (Underkategori*)kat_ptr->hentUkatListe()->remove(ukat);
	// Hvis det finnes gjenstander:
	if (ukat_ptr->hentGjenstandsListe()->noOfElements() > 0) {
		// Viser 'delvis' informasjon om alle gjenstander.
		ukat_ptr->displayGjenstander();
		// Informasjonsmelding.
		cout << "\n\tVelg gjenstandsnr. for aa se informasjon: ";
		// Leser gjenstandsnr.
		cin >> valg; cin.ignore();
		// Hvis gyldig gjenstandsnr:
		if (ukat_ptr->hentGjenstandsListe()->inList(valg)) {
			// Fjerner gjenstand fra listen.
			gjenstand_ptr = (Gjenstand*)ukat_ptr->hentGjenstandsListe()->remove(valg);
			// Hvis gjenstanden er aktiv:
			if (gjenstand_ptr->hentAktiv()) {
				// Viser all informasjon om valgt gjenstand.
				gjenstand_ptr->displayAllInfo();
				gjenstand_ptr->visBud();
			}
			// Feilmelding skrives ut.
			else {
				cout << "\n\tValgt gjenstand er IKKE aktiv!"
					<< " (trykk Enter for aa fortsette)."; cin.get();
			}
			// Legger gjenstand tilbake i listen.
			ukat_ptr->hentGjenstandsListe()->add(gjenstand_ptr);
		}
		// Hvis ugyldig gjenstandsnr:
		else {
			cout << "\n\tIngen registrerte gjenstander med valgt nr!"
				<< " (trykk Enter for aa fortsette)."; cin.get();
		}
	}
	// Hvis det ikke er opprettet noen gjenstand(er):
	else {
		cout << "\n\tIngen registrerte gjenstander"
			<< " (trykk Enter for aa fortsette)."; cin.get();
	}
	// Legger kategori tilbake i listen.
	kategorier_ptr->hentKatListe()->add(kat_ptr);
	// Legger underkategori tilbake i listen.
	kat_ptr->hentUkatListe()->add(ukat_ptr);
}

