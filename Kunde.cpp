#include "Kunde.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "lesFunks.h"

using namespace std;

Kunde::Kunde(const char* brknvn) : TextElement(brknvn){

	les("Navn", navn);	
	les("Passord", passord);
	les("Gateadresse", gateAdr);	
	les("Postadresse", postAdr);	
	les("Email", email);	
	postNr = les("Postnr",0,9999);	
	
	antSolgt = 0;
	antKjopt = 0;

	// Brukeren er ikke admin som default.
	admin = false;

	// Lag kundens kjopfil
	string e = brknvn;
	string k = "K" + e + ".DTA";
	ofstream kfil(k);

	// Lag kundens salgfil:
	k = "S" + e + ".DTA";
	ofstream sfil(k);
}

const char* Kunde::hentPassord() { 
	return passord; 
}

bool Kunde::hentAdmin() {
	return admin;
}
void Kunde::settAdmin(bool b) {
	admin = b;
}

void Kunde::skrivTilFil(ofstream &ut) {
	ut 	 << text << '\n'
		 << navn << '\n'
		 << passord << '\n'
		 << gateAdr << '\n'
		 << postAdr << '\n'
		 << email << '\n'
		 << postNr << " " 
		 << antSolgt << " " 
		 << antKjopt << " "
		 << admin << "\n";
}


void Kunde::display() {

	cout << text << '\n'
		 << navn << '\n'
		 << passord << '\n'
		 << gateAdr << '\n'
		 << postAdr << '\n'
		 << email << '\n'
		 << postNr << '\n'
		 << antSolgt << '\n'
		 << antKjopt << '\n'
		 << admin << "\n\n";

}

Kunde::Kunde(ifstream &inn, const char* brknvn) : TextElement(brknvn) {
	inn.getline(navn,STRLEN);
	inn.getline(passord,STRLEN);
	inn.getline(gateAdr,STRLEN);
	inn.getline(postAdr,STRLEN);
	inn.getline(email,STRLEN);
	inn >> postNr >> antSolgt >> antKjopt >> admin;
	inn.ignore();
	
	// Lag kundens kjopfil
	string e = brknvn;
	string k = "K" + e + ".DTA";
	ofstream kfil(k);

	// Lag kundens salgfil:
	k = "S" + e + ".DTA";
	ofstream sfil(k);
}

void Kunde::oekAntKjop() {
	antKjopt++;
}

void Kunde::oekAntSalg() {
	antSolgt++;
}