#ifndef __BUD_H
#define __BUD_H

#include "ListTool2B.h"
#include "const.h"
#include <fstream>
using namespace std;

// Bud er sortert etter budets størrelse:
class Bud : public NumElement {
private:
	List* budListe;
	char brukernavn[STRLEN];  // Brukernavn.
	int min;                  // Minutt.
	int tim;                  // Time.
	int dag;                  // Dag.
	int mnd;                  // Måned.
	int aar;                  // År.
	int budgjNr;		      // Gjenstandsnr. brukt i bud.
	int budPris;		      // Nåværende pris på gjenstand.
	int nyBudPris;		      // Ny pris på bud.
	long long int tidspunkt;  // Tidspunkt.		

public:
	// Constructor
	Bud();
	// Pris sendes opp som ID-nummer.
	Bud(int pris);
	// Constructor leser fra fil.
	Bud(ifstream &inn, int pris);
	// Viser bud.
	void display();
	// Skriver bud til fil.
	void skrivTilFil(ofstream &ut);
	// Henter brukernavn.
	char* hentBruker();
};

#endif