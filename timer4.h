#if !defined(__TIMER4_H)
#define  __TIMER4_H

#include <ctime>

class Timer  {
  private:
    tm*  tidspunkt;

  public: 
	  Timer();
    void hent(int& dag, int& mnd, int& aar, int& time, int& min);
	void konverterFraDagFrst(long long int& dato);
	int konverterFraTm(int& dag, int& mnd, int& aar, int& tim, int& min);
	void displayTidspunkt(long long int dato);
};

#endif


