#ifndef __UNDERKATEGORI_H
#define __UNDERKATEGORI_H

#include "ListTool2B.h"
#include "globalVar.h"
#include <fstream>

// Klasse for Underkategori. Sorteres p� kategorinavn.
class Underkategori : public TextElement {

private:

	int ukategoriNr;        // Kategorinr. for underkategorielement.
	List* gjenstandsListe;  // Liste for gjenstander.

public:

	// Constructor -> char g�r til textelement, int = kategorinr:
	Underkategori(const char* txt, int nr);
	// Skriv underkategori til fil.
	void skrivTilFil(std::ofstream &ut);
	// Hent underkategori nr.
	int hentKatNr();
	// Henter gjenstandliste.
	List* hentGjenstandsListe();
	// Display underkategori.
	void display();
	// Displayer alle gjenstander i en underkategori:
	void displayGjenstander();
	void skrivGjenstanderTilFil();
	void lesGjenstanderFraFil();
};
#endif