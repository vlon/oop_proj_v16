#ifndef __KUNDER_H
#define __KUNDER_H

#include "ListTool2B.h"
#include "globalVar.h"
#include "const.h"

class Kunder {										// Kunder klassen holder oversikt over alle kunder og innlogget bruker.

private:

	static char* innloggetBruker; 					// Nåværende innlogget bruker.
	List* kundeListe;								// Liste med kunder.
		
public:		

	char* hentInnloggetBruker() const ;				// Henter nåværende innlogget bruker.
	void settInnloggetBruker(const char* brknvn);	// Setter bruker som nåværende innlogget bruker
	List* hentKundeListe() const ;					// Henter kundelisten.
		
	Kunder();										// Constructor som setter liste og innloggetbruker.
	~Kunder();										// Destructor sletter liste pointer.
	void nyKunde();									// Lag ny kunde.
	void logInn();									// Log inn.
	void logUt() const;								// Log ut.
	void lesKunderFraFil();							// Leser inn alle kunder fra fil.
	void skrivKunderTilFil() const;					// Skriv alle kunder til fil.
	void endreRettigheter();						// Endre rettigheter på andre kunder(Admin funksjon).
	void seKjoptSolgt();							// Se alle egne kjøp og salg.
	void seKjopt();									// Se alle egne kjøp.
	void seSolgt();									// Se alle egne salg.
	void betalGjenstand();							// Betal vunnet auksjon/gjenstand.
	void giTilbakemelding();						// Gi tilbakemelding til annen kunde.
	void addAntKjop(const char* brukerKjop);
	void addAntSalg(const char* brukerSalg);
	void skrivKjoptSolgtTilFil(						// Oppdaterer til K/S-filene.
		const char* selgerNavn, const char* brukerKjop, 
		const char* tittel, long long int sluttTidspkt, 
		int gjeldendeBud, int porto, int number);
};

#endif