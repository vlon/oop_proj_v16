#ifndef __CONST_H
#define __CONST_H

const int STRLEN = 200;
const int MAXPRIS = 1000000000;
const int MAXOKNING = 1000000;
const int MAXPORTO = 100000;
const int MAXDATO = 3112172359;
#endif