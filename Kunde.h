#ifndef __KUNDE_H
#define __KUNDE_H

#include <fstream>								// ofstream, ifstream
#include "ListTool2B.h"							// list, textelement
#include "const.h"								// STRLEN

class Kunde : public TextElement {				// Brukernavnet blir sent opp som ID-tekst.

private:

	char navn[STRLEN];										// Kundes reele navn.
	char passord[STRLEN];									// Kundes passord.
	char gateAdr[STRLEN];									// Kundes gateadresse.
	char postAdr[STRLEN];									// Kundes postadresse.
	char email[STRLEN];										// Kundes mail.
	int postNr;												// Kundes postnr.
								
	int antSolgt;											// Antall gjenstander solgt.
	int antKjopt;											// Antall gjenstander kjøpt.
								
	bool admin;												// Er bruker admin?

public:

	Kunde(const char* brknvn); 								// Constructor som leser inn fra bruker.
	Kunde(std::ifstream& inn, const char* brknvn); 			// Constructor som leser inn fra fil.

	void skrivTilFil(std::ofstream &ut);
	void display();											// Display kundes info.
	const char* hentPassord();								// Henter passord.
	bool hentAdmin();										// Henter admin.
	void settAdmin(bool b);									// Setter admin.
	void oekAntKjop();										// Øker antall kjopt med 1.
	void oekAntSalg();										// Øker antall solgt med 1.

};

#endif