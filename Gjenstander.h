#ifndef __GJENSTANDER_H
#define __GJENSTANDER_H

#include "ListTool2B.h"
#include "const.h"

class Gjenstander {

private:
	static int gjNr;

public:
	Gjenstander();
	void nyGjenstand(char* katNr, char* ukatNr);
	void settGjNr(int nr);
	void nyttBud(char* kat, char* ukat);
	
};

#endif