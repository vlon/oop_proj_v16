#include "Kunder.h"
#include "Kunde.h"
#include <iostream>
#include <cstring>
#include <fstream>
#include <string>
#include "globalVar.h"
#include <stdio.h>
#include "const.h"
#include "lesFunks.h"
#include <cctype>

using namespace std;

char* Kunder::innloggetBruker;

char brknvn[STRLEN] = "";								// Variable for brukernavn.

Kunder::Kunder() {
	kundeListe = new List(Sorted);
}

Kunder::~Kunder() {
	delete innloggetBruker;
	delete kundeListe;				
}

void Kunder::nyKunde() {

	// Les inn alle kunder inn i minne.
	lesKunderFraFil();

	// Hvis ikke innlogget:
	if (!innlogget) {
		cout << endl;
		// Leser inn brukernavn.
		les("Brukernavn",brknvn);
		
		// Hvis brukernavnet eksisterer
		if (kundeListe->inList(brknvn)) {
			// Skriver ut melding og spør om brukeren skal logge inn.
			les("Bruker finnes allerede. Prov på nytt(y/n)",ch);
			if (ch == 'Y') { nyKunde(); }

		// Hvis ikke brukernavnet eksisterer:
		} else {
			// Lag ny kunde.
			kunde_ptr = new Kunde(brknvn);
			// Legg kunden til i kundelisten.
			kundeListe->add(kunde_ptr);
			// Gjør om til nåværende innlogget bruker:
			settInnloggetBruker(brknvn);
			innlogget = true;
			// Skriv kunder til fil.
			skrivKunderTilFil();
		}
	}
}

// Returnerer hvilken bruker som er innlogget:
char* Kunder::hentInnloggetBruker() const {
	return innloggetBruker;
}

// Setter bruker som nåværende innlogget bruker
void Kunder::settInnloggetBruker(const char* brknvn) {
	innloggetBruker = new char[strlen(brknvn)+1];
	strcpy(innloggetBruker, brknvn);
}

// Henter kundelisten:
List* Kunder::hentKundeListe() const {
	return kundeListe;
}

void Kunder::logInn() {

	// Les inn alle kunder inn i minne.
	lesKunderFraFil();	

	// Bruker må være utlogget for å kunne logge inn.
	if (!innlogget) {

		// Spør etter brukernavn.
		les("Brukernavn",brknvn);
		
		// Sjekk om bruker finnes i listen
		if (kundeListe->inList(brknvn)) {			
			// Hent ut bruker(fjernes fra listen).
			kunde_ptr = (Kunde*)kundeListe->remove(brknvn);
			// Legg kunden tilbake i listen.
			kundeListe->add(kunde_ptr);
		}

		// Spør etter passord.
		char psrd[STRLEN];
		les("Passord",psrd);	

		// Hvis brukernavn finnes og passord er rett..
		if(kundeListe->inList(brknvn) && strcmp(psrd,kunde_ptr->hentPassord()) == 0) {

			// Sett brukernavn til nåværende innlogget bruker.
			settInnloggetBruker(brknvn);

			// Hvis kunden er admin..
			if (kunde_ptr->hentAdmin()) { 
				// Global admin status settes til true.
				admin = true; 
			}

			// Global Innlogget status settes til true.
			innlogget = true;

		// Tilfelle feil brukernavn og passord.
		} else {

			// Spør bruker om å prøve på nytt
			les("Ugydlig brukernavn/passord. Prov på nytt(y/n)",ch);

			// Hvis y
			if (ch == 'Y') { 
				// Kjør logInn på nytt.
				logInn();
			}
		}
	}
}

void Kunder::logUt() const {
	innlogget = false;
	admin = false;
}

void Kunder::lesKunderFraFil() {	
																			
	string fil = "KUNDER.DTA";													// Filen som data leses inn fra.

	ifstream inn(fil);
																			
	if (inn) {																	// Hvis filen kan åpnes
																				// Så lenge neste char i filen ikke er end of file.
		while (inn.peek() != -1) {
			inn.getline(brknvn,STRLEN);											// Les inn brukernavn.
			kunde_ptr = new Kunde(inn, brknvn);									// Lag ny kunde.
			kundeListe->add(kunde_ptr);											// Legg kunden til i listen.
		}
	}
}

// Skriver kundenes data fil fil:
void Kunder::skrivKunderTilFil() const {	

	// Filen som data skrives til.
	string fil = "KUNDER.DTA";

	ofstream ut(fil);

	// Hvis filen kan åpnes:
	if (ut) {
		// Finner og går igjennom ant. eksisterende kunder:
		for (int i = 1; i <= kundeListe->noOfElements(); i++) {
			// Hent ut kunde(fjernes fra listen).
			kunde_ptr = (Kunde*)kundeListe->removeNo(i);
			// Ber kunden skrive seg selv til fil.
			kunde_ptr->skrivTilFil(ut);
			// Legg kunden tilbake i listen.
			kundeListe->add(kunde_ptr);
		}
	}
}

// Endre rettighet til en annen bruker (BARE FOR ADMIN):
void Kunder::endreRettigheter() {

	// Les inn alle kunder fra fil.
	lesKunderFraFil();				

	// Hvis brukeren er innlogget og admin..
	if (innlogget && admin) {

		// Les inn et brukernavn fra brukeren.
		les("Endre rettigheter på bruker", brknvn);

		if (kundeListe->inList(brknvn)) {
	
			// Sjekker at innlogget bruker er ulik angitt bruker:
			if (strcmp(innloggetBruker, brknvn) != 0) {
				// Hent ut inntastet bruker (fjernes fra listen).
				kunde_ptr = (Kunde*)kundeListe->remove(brknvn);
		
				// Leser inn y/n fra bruker.
				les("Admin(y/n)",ch);

				// Hvis 'y' sett som ADMIN.
				if(ch == 'Y') {
					cout << '\t' << brknvn << " er nå admin.\n";
					kunde_ptr->settAdmin(true);
				// Eller hvis 'n' sett som IKKE ADMIN.
				} else if (ch == 'N'){
					cout << '\t' << brknvn << " er nå ikke admin.\n";		
					kunde_ptr->settAdmin(false);
				}
				// Legg tilbake i listen.
				kundeListe->add(kunde_ptr);

				// Skriv kunder til fil.
				skrivKunderTilFil();

				// Tilfelle innskrevne navn er likt innlogget bruker..
			} else  {
				les("Du kan ikke endre på dine egne rettigheter. Prov på nytt(y/n)",ch);
				if (ch == 'Y') endreRettigheter();
			}

		// Tilfelle brukeren ikke finnes..
		} else {
			les("Brukeren finnes ikke. Prov på nytt(y/n)",ch);
			if (ch == 'Y') endreRettigheter();
		}

	} else {
		// Legg innlogget bruker tilbake i listen.
		kundeListe->add(kunde_ptr);
	}
}

void Kunder::seKjopt() {
	// Åpne kundens kjøp fil:
	string e = brknvn;
	string k = "K" + e + ".DTA";
	ifstream k_inn(k);

	if (k_inn) {

		cout << "\n\tALLE KJOP:\t\t" << endl;

		while (k_inn.peek() != -1) {
			// INT for tidspunkt og char variable for å lese fra fil.
			int tid;
			char s[STRLEN];
			// Leser inn tidspunkt.
			k_inn >> tid; k_inn.ignore();
			cout << "\n\t\t\tTidspunkt:\t";
			// Skriver ut tidspunkt på formen: DD.MM.ÅÅ TT:MM
			timer_ptr->displayTidspunkt(tid);
			// SELGER:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tSelger:\t\t\t" << s << endl;
			// Hopp over kjoper:
			k_inn.getline(s, STRLEN);
			// GJENSTAND
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tGjenstand:\t\t" << s << endl;
			// PRIS:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tPris:\t\t\t" << s << endl;
			// GJENSTANDSNUMMER:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tGjenstandsnummer:\t" << s << endl;
			// BETALT:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tBetalt:\t\t\t" << s << endl;
			// KARAKTER:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tKarakter:\t\t" << s << endl;
			// FEEDBACK:
			k_inn.getline(s, STRLEN);
			cout << "\t\t\tFeedback:\t\t" << s << "\n\n";
		}
	}
	else cout << "\tFilen " << k << " kunne ikke åpnes.\n";
}

void Kunder::seSolgt() {
	string e = brknvn;
	string k = "S" + e + ".DTA";
	ifstream s_inn(k);

	if (s_inn) {

			cout << "\n\tALLE SALG:\t\t" << endl;

			// Skriv ut all innhold.
			while (s_inn.peek() != -1) {
				// INT for tidspunkt og char variable for å lese fra fil.
				int tid;
				char s[STRLEN];
				// Leser inn tidspunkt:
				s_inn >> tid; s_inn.ignore();
				cout << "\n\t\t\tTidspunkt:\t";
				// Skriver ut tidspunkt på formen : DD.MM.ÅÅ TT:MM
				timer_ptr->displayTidspunkt(tid);
				// Hopp over selger:
				s_inn.getline(s, STRLEN);
				// KJOPER:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tKjoper:\t\t\t" << s << endl;
				// GJENSTAND:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tGjenstand:\t\t" << s << endl;
				// PRIS:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tPris:\t\t\t" << s << endl;
				// GJENSTANDSNUMMER:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tGjenstandsnummer:\t" << s << endl;
				// BETALT:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tBetalt:\t\t\t" << s << endl;
				// KARAKTER:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tKarakter:\t\t" << s << endl;
				// FEEDBACK:
				s_inn.getline(s, STRLEN);
				cout << "\t\t\tFeedback:\t\t" << s << "\n\n";
			}
	
		} else cout << "\tFilen " << k << " kunne ikke åpnes.\n";
}

void Kunder::seKjoptSolgt() {

	// Hvis innlogget:
	if (innlogget) {
		seKjopt();
		seSolgt();
	}
}

void Kunder::betalGjenstand() {
	// Hvis brukeren er innlogget:
	if (innlogget) {
		lesKunderFraFil();
		// Vis alle kjøpte gjenstander til kunden
		seKjopt();
		// Les inn gjnr
		int gjnr;
		char brukernavn[STRLEN];
		les("Navn på selger", brukernavn);

		if (kundeListe->inList(brukernavn)) {
			if (strcmp(innloggetBruker, brukernavn) != 0) {
				// Spør bruker om gjenstandsnr
				cout << "\tGjenstand nr: "; cin >> gjnr; cin.ignore();
				// Spør bruker om å taste inn beløp
				int belop = 0;
				do {
					cout << "\tBeløp: "; cin >> belop; cin.ignore();
				} while (belop < MAXPRIS);

				// Åpne filen til kjøper
				string s = innloggetBruker;
				string fil = "K" + s + ".DTA";
				ifstream k_inn(fil);
				ofstream k_out("temp.DTA");

				// Hvis filen kunne åpnes..
				if (k_inn) {

					bool found = false;
					string temp;

					// Fortsett å les til slutten av filen
					while (k_inn.peek() != -1) {

						// Skriver betalt
						if (found) {
							getline(k_inn, temp, '\n');
							temp = "BETALT";
							found = false;

						}
						else {

							// Les inn en linje og sammenlign med gjnr..
							getline(k_inn, temp, '\n');
						}

						// Hvis det stemmer skriv over neste linje med betalt.
						if (temp == to_string(gjnr)) {
							found = true;
						}

						// Legg på en newline
						temp += '\n';

						// Skriv til fil
						k_out << temp;
					}
				}
				rename("temp.DTA", fil.c_str());

				// Åpne filen til selgeren
				s = brukernavn;
				fil = "S" + s + ".DTA";
				ifstream s_inn(fil);
				ofstream s_out("temp.DTA");

				// Hvis filen kunne åpnes..
				if (s_inn) {

					bool found = false;
					string temp;

					// Fortsett å les til slutten av filen
					while (s_inn.peek() != -1) {

						// Skriver betalt
						if (found) {
							getline(s_inn, temp, '\n');
							temp = "BETALT";
							found = false;

						}
						else {

							// Les inn en linje og sammenlign med gjnr..
							getline(s_inn, temp, '\n');
						}

						// Hvis det stemmer skriv over neste linje med betalt.
						if (temp == to_string(gjnr)) {
							found = true;
						}

						// Legg på en newline
						temp += '\n';

						// Skriv til fil
						s_out << temp;
					}
				}
				// Overskriv filen med data fra temp
				rename("temp.DTA", fil.c_str());
			}
			else {
				cout << "\n\tIKKE tillatt aa betale til seg selv!"
					<< " (trykk 'Enter for aa fortsette)."; cin.get();
			}
		}
		else {
			cout << "\n\tIngen registrert bruker med dette navnet!"
				<< " (trykk 'Enter for aa fortsette)."; cin.get();
		}
	}
	else {
		cout << "\n\tIngen bruker innlogget!"
			<< " (trykk 'Enter for aa fortsette)."; cin.get();
	}
}


void Kunder::giTilbakemelding() {
	if (innlogget) {
		lesKunderFraFil();
		// Vis alle kjøpte gjenstander til kunden
		seKjopt();

		// Les inn gjnr
		int gjnr;
		char brukernavn[STRLEN];
		les("Gi tilbakemelding til bruker", brukernavn);

		if (strcmp(innloggetBruker, brukernavn) != 0) {
			if (kundeListe->inList(brukernavn)) {

				// Leser inn om det er kjøp eller slag
				char i;
				les("Kjøp eller salg?(K/S)", i);

				cout << "\tGjenstand nr: "; cin >> gjnr; cin.ignore();

				// Les inn karakter
				int karakter = les("Karakter(1-6)", 1, 6);

				// Les inn tilbakemelding
				char melding[STRLEN];
				les("Tilbakemelding(200tegn)", melding);

				// Åpne kjøpers
				string s = brukernavn;
				i = toupper(i);
				string fil = to_string(i) + s + ".DTA";
				ifstream inn(fil);
				ofstream out("temp.DTA");

				// Hvis filen kunne åpnes..
				if (inn) {

					bool found = false;
					string temp;

					// Fortsett å les til slutten av filen
					while (inn.peek() != -1) {

						// Skriver karakter og melding
						if (found) {

							// Hopp over en linje
							getline(inn, temp, '\n');
							temp += '\n';
							out << temp;

							// Skriv ut karakter
							getline(inn, temp, '\n');
							temp = to_string(karakter);
							temp += '\n';
							out << temp;

							// Skriv ut melding
							getline(inn, temp, '\n');
							temp = melding;
							temp += '\n';
							out << temp;

						}
						else {

							getline(inn, temp, '\n');

							// Sjekker om vi har kommen frem til gjnr..
							if (temp == to_string(gjnr)) {
								found = true;
							}

							// Skriv til fil
							temp += '\n';
							out << temp;
						}
					}
				}
				// Overskriv filen med data fra temp
				rename("temp.DTA", fil.c_str());
			}
			else {
				cout << "\n\tIngen registrert bruker med dette navnet!"
					<< " (trykk 'Enter for aa fortsette)."; cin.get();
			}
		}
		else {
			cout << "\n\tIKKE tillatt aa gi tilbakemelding til seg selv!"
				<< " (trykk 'Enter for aa fortsette)."; cin.get();
		}
	}
	else {
		cout << "\n\tIngen bruker innlogget!"
			<< " (trykk 'Enter for aa fortsette)."; cin.get();
	}
}

void Kunder::addAntKjop(const char* brukerKjop) {
	// Finner og går igjennom ant. eksisterende kunder:
	if (kundeListe->inList(brukerKjop)) {
		for (int i = 1; i <= kundeListe->noOfElements(); i++) {
				// Hent ut kunde(fjernes fra listen).
				kunde_ptr = (Kunde*)kundeListe->removeNo(i);
				// Kundens antkjop plusses med 1.
				kunde_ptr->oekAntKjop();
				// Legg kunden tilbake i listen.
				kundeListe->add(kunde_ptr);
		}
	}
}

void Kunder::addAntSalg(const char* brukerSalg) {
	// Finner og går igjennom ant. eksisterende kunder:
	if (kundeListe->inList(brukerSalg)) {
		for (int i = 1; i <= kundeListe->noOfElements(); i++) {
			// Hent ut kunde(fjernes fra listen).
			kunde_ptr = (Kunde*)kundeListe->removeNo(i);
			// Kundens antkjop plusses med 1.
			kunde_ptr->oekAntSalg();
			// Legg kunden tilbake i listen.
			kundeListe->add(kunde_ptr);
		}
	}
}

void Kunder::skrivKjoptSolgtTilFil(
	const char* selgerNavn, const char* brukerKjop, 
	const char* tittel, long long int sluttTidspkt, 
	int gjeldendeBud, int porto, int number) {

	// Legger til kjøp i kjøpers K-fil.
	string e = brukerKjop;
	string k = "K" + e + ".DTA";
	ofstream k_inn(k, ios::app);

	// Skriver kjøp til fil
	if (k_inn) {
		k_inn << sluttTidspkt << endl;
		k_inn << selgerNavn << endl;
		k_inn << brukerKjop << endl;
		k_inn << tittel << endl;
		k_inn << (gjeldendeBud + porto) << endl;
		k_inn << number << endl;
		k_inn << "IKKE BETALT" << endl;
		k_inn << "" << endl;
		k_inn << "" << endl;
	}
	else cout << "\n\tFilen " << k << " kunne ikke åpnes.\n";

	e = selgerNavn;
	k = "S" + e + ".DTA";
	ofstream s_inn(k, ios::app);

	// Skriver kjøp til fil
	if (s_inn) {
		s_inn << sluttTidspkt << endl;
		s_inn << selgerNavn << endl;
		s_inn << brukerKjop << endl;
		s_inn << tittel << endl;
		s_inn << (gjeldendeBud + porto) << endl;
		s_inn << number << endl;
		s_inn << "IKKE BETALT" << endl;
		s_inn << "" << endl;
		s_inn << "" << endl;
	}
	else cout << "\n\tFilen " << k << " kunne ikke åpnes.\n";
}



