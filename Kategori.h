#ifndef __KATEGORI_H
#define __KATEGORI_H

#include "ListTool2B.h"
#include <fstream>
#include "const.h"

// Klasse for Kategori. Sorteres p� kategorinavn.
class Kategori : public TextElement {

private:
	int   kategoriNr;                  // Kategorinr. for kategorielement.
	List* underkategoriListe;          // Liste for underkategorielementer.
	
public:

	// Constructor med char og int.
	Kategori(const char* txt, int nr);
	// Henter listen for underkategoriene.
	List* hentUkatListe();
	// Skrive kategori/underkategori til fil.
	void skrivTilFil(std::ofstream &ut);
	// Display hovedkategorier.
	void display();
	// Display alle underkategorier i en kategori.
	void displayAll();
	// Henter kategorinr.
	int hentKatNr();
};
#endif