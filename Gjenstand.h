#ifndef __GJENSTAND_H
#define __GJENSTAND_H

#include "ListTool2B.h"
#include "const.h"
#include <fstream>
#include "timer4.h"

using namespace std;

class Gjenstand : public NumElement {	// gjNr er identifikator

private:
	List* budListe;				// Liste over alle bud.
	char selgerNavn[STRLEN];	// Selgers brukernavn
	char gjTittel[STRLEN];		// Gjenstandens tittel.
	char beskrivelse[STRLEN];	// Beskrivelse av gjenstanden.
	long long int startTidspkt;	// Starttidspunkt
	long long int sluttTidspkt;	// Sluttidspunkt.
	int startPris;				// Startpris.
	int porto;					// Porto.
	int budOkn;					// Bud�kning.
	int antBud;					// antall bud.
	int gjeldendeBud;			// N�v�rende bud.
	bool aktiv;					// Om auksjon er aktiv.

public:

	Gjenstand(int gjNr); 							// Constructor som leser inn fra gjenstandsID.
	Gjenstand(ifstream& inn, int gjNr); 			// Constructor som leser inn fra fil.
	void skrivTilFil(ofstream &ut);

	void display();
	void displayAllInfo();
	void visBud();
	void sjekkAktiv();
	List* hentBudListe();
	bool hentAktiv();
	int hentStartPris();
	int hentbudOkn();
	int hentgjeldendeBud();
	const char* hentKjoper(int gjNr);
	const char* hentNvn();
	int hentAntBud();
	void settGjeldendeBud(int bud);
	void oekAntBud();
	
	void auksjonFerdig();
	
};

#endif