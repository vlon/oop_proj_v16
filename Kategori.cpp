#include "Kategori.h"
#include "Kategorier.h"
#include "globalVar.h"
#include "lesFunks.h"
#include "Underkategori.h"
#include <iostream>
#include "const.h"

using namespace std;

// Constructor -> char g�r til textelement, int = kategorinr:
Kategori::Kategori(const char* txt, int nr) : TextElement(txt) {
	kategoriNr = nr;
	// Ny sortert liste for underkategorier.
	underkategoriListe = new List(Sorted);
}

// Returnerer underkategorilisten:
List* Kategori::hentUkatListe() {
	return underkategoriListe;
}

// Skriver hoved- og underkategoriene til fil:
void Kategori::skrivTilFil(ofstream &ut) {
	// INT for ant. elementer i listen og for-l�kken.
	int i, j;

	// Finner ant. elementer i listen.
	i = underkategoriListe->noOfElements();

	// Skriver ut kategorinr., navn p� kategori og antall underkategorier.
	ut << kategoriNr << " " << text << '\n' << i << '\n';
	// Hvis det finnes underkategori:
	if (i > 0) {
		// G�r igjennom alle underkategoriene.
		for (j = 1; j <= i; j++) {
			// Hent ut kunde (fjernes fra listen).
			ukat_ptr = (Underkategori*)underkategoriListe->removeNo(j);
			// Underkategori skrives til fil.
			ukat_ptr->skrivTilFil(ut);
			// Underkategori legges tilbake i listen.
			underkategoriListe->add(ukat_ptr);
		}
	}
}                                           
                                          
// Display hovedkategori:
void Kategori::display() {
	// Skriver ut kategorinr. og navn p� kategori.
	cout << "\t" << text << endl;
}

// Display alle underkategorier i en kategori:
void Kategori::displayAll() {
	// G�r igjennom alle underkategoriene i kategorien:
	if (underkategoriListe->noOfElements() > 0) {
		for (int i = 1; i <= underkategoriListe->noOfElements(); i++) {
			// Henter hver underkategori (fjerner fra listen).
			ukat_ptr = (Underkategori*)underkategoriListe->removeNo(i);
			// Display underkategori.
			ukat_ptr->display();
			// Legger underkategorien tilbake i listen.
			underkategoriListe->add(ukat_ptr);
		}
	}
	else {
		cout << "\t(Ingen underkategorier er opprettet ennaa)" << endl;
	}
}

// Henter kategorinummer:
int Kategori::hentKatNr() {
	return kategoriNr;
}
